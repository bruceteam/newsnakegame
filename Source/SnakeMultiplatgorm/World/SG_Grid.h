// SnakeGamelifeEXEtrib

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeMultiplatgorm/World/SG_WorldTypes.h"
#include "SnakeMultiplatgorm/Core/SnakeTypes.h"
#include "SG_Grid.generated.h"

namespace SnakeGame
{
	class Grid;
}

class UStaticMeshComponent;

UCLASS()
class SNAKEMULTIPLATGORM_API ASG_Grid : public AActor
{
	GENERATED_BODY()
	
public:	
	ASG_Grid();
	virtual void Tick(float DeltaTime) override;
	void SetModel(const TSharedPtr<SnakeGame::Grid>& Grid, uint32 InCellSize);

	void UpdateColors(const FSnakeColors& ColorSet);
protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
		USceneComponent* Origin;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* GridMesh;
private:
	UPROPERTY()
		UMaterialInstanceDynamic* GridMaterial;

	SnakeGame::Dim GridDim;
	uint32 CellSize;
	uint32 WorldWidth;
	uint32 WorldHeight;

	void DrawGrid();
};
