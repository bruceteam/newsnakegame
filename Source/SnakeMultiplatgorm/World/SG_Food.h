// SnakeGamelifeEXEtrib

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeMultiplatgorm/Core/Food.h"
#include "SG_Food.generated.h"

class UStaticMeshComponent;
class UNiagaraSystem;

UCLASS()
class SNAKEMULTIPLATGORM_API ASG_Food : public AActor
{
	GENERATED_BODY()
	
public:	
	ASG_Food();

	void SetModel(const TSharedPtr<SnakeGame::Food>& InFood, uint32 InCellSize, const SnakeGame::Dim& InDims);
	void UpdateColor(const FLinearColor& Color);

	virtual void Tick(float DeltaTime) override;

	void Explode();
	void Hide();
protected:	
	UPROPERTY(VisibleAnywhere)
		USceneComponent* Origin;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* FoodMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category  = "FX")
		TObjectPtr<UNiagaraSystem> ExplosionEffect;
private:
	TWeakPtr<SnakeGame::Food> Food;
	uint32 CellSize;
	SnakeGame::Dim Dims;
	FLinearColor FoodColor;

	FVector GetFoodWorldLocation() const;
};
