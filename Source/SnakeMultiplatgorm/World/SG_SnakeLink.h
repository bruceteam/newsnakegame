// SnakeGamelifeEXEtrib

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SG_SnakeLink.generated.h"

class UStaticMeshComponent;
class UNiagaraSystem;

UCLASS()
class SNAKEMULTIPLATGORM_API ASG_SnakeLink : public AActor
{
	GENERATED_BODY()
	
public:	
	ASG_SnakeLink();

	void UpdateColors(const FLinearColor& Color);
	void UpdateScale(int32 CellSize);
	void Explode();

	UPROPERTY(VisibleAnywhere)
		USceneComponent* Origin;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* LinkMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category  = "FX")
		TObjectPtr<UNiagaraSystem> ExplosionEffect;
private:
		FLinearColor LinkColor;
};
