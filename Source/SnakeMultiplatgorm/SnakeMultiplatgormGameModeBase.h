// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeMultiplatgormGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEMULTIPLATGORM_API ASnakeMultiplatgormGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
