// SnakeGamelifeEXEtrib

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SG_MenuGameMode.generated.h"


UCLASS()
class SNAKEMULTIPLATGORM_API ASG_MenuGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	virtual void StartPlay() override;
};
