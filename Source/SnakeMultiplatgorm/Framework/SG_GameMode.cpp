// SnakeGamelifeEXEtrib


#include "SnakeMultiplatgorm/Framework/SG_GameMode.h"
#include "SnakeMultiplatgorm/Core/SnakeTypes.h"
#include "SnakeMultiplatgorm/Core/Grid.h"
#include "SnakeMultiplatgorm/World/SG_Grid.h"
#include "SnakeMultiplatgorm/World/SG_Snake.h"
#include "SnakeMultiplatgorm/World/SG_Food.h"
#include "SnakeMultiplatgorm/World/SG_WorldTypes.h"
#include "SnakeMultiplatgorm/Framework/SG_Pawn.h"
#include "Engine/ExponentialHeightFog.h"
#include "Components/ExponentialHeightFogComponent.h"
#include "Kismet/GameplayStatics.h"
#include "EnhancedInputSubsystems.h"
#include "SnakeMultiplatgorm/UI/SG_HUD.h"
#include "EnhancedInputComponent.h"
#include "SnakeMultiplatgorm/World/SG_WorldUtils.h"
#include "SnakeMultiplatgorm/Framework/SG_GameUserSettings.h"

DEFINE_LOG_CATEGORY_STATIC(LogSnakeGameMode, All, All);

ASG_GameMode::ASG_GameMode()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ASG_GameMode::StartPlay()
{
	Super::StartPlay();

	//init core game
	Game = MakeShared<SnakeGame::Game>(MakeSettings());
	check(Game.IsValid());
	SubscribeOnGameEvents();

	//init world grid
	const FTransform GridOrigin = FTransform::Identity;
	check(GetWorld());
	GridVisual = GetWorld()->SpawnActorDeferred<ASG_Grid>(GridVisualClass, GridOrigin);
	check(GridVisual);
	GridVisual->SetModel(Game->grid(), CellSize);
	GridVisual->FinishSpawning(GridOrigin);

	//init world snake
	SnakeVisual = GetWorld()->SpawnActorDeferred<ASG_Snake>(SnakeVisualClass, GridOrigin);
	SnakeVisual->SetModel(Game->snake(), CellSize, Game->grid()->dim());
	SnakeVisual->FinishSpawning(GridOrigin);

	//init world food
	FoodVisual = GetWorld()->SpawnActorDeferred<ASG_Food>(FoodVisualClass, GridOrigin);
	FoodVisual->SetModel(Game->food(), CellSize, Game->grid()->dim());
	FoodVisual->FinishSpawning(GridOrigin);

	//Find ExponentialHeightFog in World;
	FindFog();

	//updateColors
	check(ColorsTable);
	const auto RowsCount = ColorsTable->GetRowNames().Num();
	check(RowsCount >= 1);
	ColorTableIndex = FMath::RandRange(0, RowsCount - 1);
	UpdateColors();

	SetupInput();

	//UMG
	auto* PC = GetWorld()->GetFirstPlayerController();
	check(PC);
	HUD = Cast<ASG_HUD>(PC->GetHUD());
	check(HUD);
	HUD->SetModel(Game);
	const FString ResetGameKeyName = SnakeGame::WorldUtils::FindActionKeyName(InputMapping, ResetGameInputAction);
	HUD->SetInputKeyNames(ResetGameKeyName);

	SnakeGame::WorldUtils::SetUIIput(GetWorld(), false);
	
	//set pawn location
	auto* Pawn = Cast<ASG_Pawn>(PC->GetPawn());
	check(Pawn);
	check(Game->grid().IsValid());
	Pawn->UpdateLocation(Game->grid()->dim(), CellSize, GridOrigin);
}

//from console, not in shipping build
void ASG_GameMode::NextColor()
{
	if (ColorsTable)
	{
		ColorTableIndex = (ColorTableIndex + 1) % ColorsTable->GetRowNames().Num();
		UpdateColors();
	}
}

void ASG_GameMode::FindFog()
{
	TArray<AActor*> Fogs;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AExponentialHeightFog::StaticClass(), Fogs);
	if (Fogs.Num() > 0)
	{
		Fog = Cast<AExponentialHeightFog>(Fogs[0]);
	}
}

void ASG_GameMode::UpdateColors()
{
	const auto RowName = ColorsTable->GetRowNames()[ColorTableIndex];
	const auto* ColorSet = ColorsTable->FindRow<FSnakeColors>(RowName, {});
	if (ColorSet)
	{
		GridVisual->UpdateColors(*ColorSet);
		SnakeVisual->UpdateColors(*ColorSet);
		FoodVisual->UpdateColor(ColorSet->FoodColor);

		//update scene ambient color
		if (Fog && Fog->GetComponent())
		{
			Fog->GetComponent()->SkyAtmosphereAmbientContributionColorScale = ColorSet->SkyAtmosphereColor;
			Fog->MarkComponentsRenderStateDirty();
		}

	}
}

void ASG_GameMode::SetupInput()
{
	if (!GetWorld()) return;

	if (auto* PC = GetWorld()->GetFirstPlayerController())
	{
		if (auto* InputSystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PC->GetLocalPlayer()))
		{
			InputSystem->AddMappingContext(InputMapping, 0);
		}

		auto* Input = Cast<UEnhancedInputComponent>(PC->InputComponent);
		check(Input);
		Input->BindAction(MoveForwardInputAction, ETriggerEvent::Started, this, &ThisClass::OnMoveForward);
		Input->BindAction(MoveRightInputAction, ETriggerEvent::Started, this, &ThisClass::OnMoveRight);
		Input->BindAction(ResetGameInputAction, ETriggerEvent::Started, this, &ThisClass::OnResetGame);
	}
}

void ASG_GameMode::OnMoveForward(const FInputActionValue& Value)
{
	const float InputValue = Value.Get<float>();
	if (InputValue == 0.0f) return;
	SnakeInput = SnakeGame::Input{ 0, static_cast<int8>(InputValue) };
}

void ASG_GameMode::OnMoveRight(const FInputActionValue& Value)
{
	const float InputValue = Value.Get<float>();
	if (InputValue == 0.0f) return;
	SnakeInput = SnakeGame::Input{ static_cast<int8>(InputValue), 0 };
}

void ASG_GameMode::OnResetGame(const FInputActionValue& Value)
{
	if (const bool InputValue = Value.Get<bool>())
	{
		Game = MakeShared<SnakeGame::Game>(MakeSettings());
		check(Game.IsValid());
		SubscribeOnGameEvents();
		GridVisual->SetModel(Game->grid(), CellSize);
		SnakeVisual->SetModel(Game->snake(), CellSize, Game->grid()->dim());
		FoodVisual->SetModel(Game->food(), CellSize, Game->grid()->dim());
		HUD->SetModel(Game);
		SnakeInput = SnakeGame::Input::Default;
		NextColor();
		SnakeGame::WorldUtils::SetUIIput(GetWorld(), false);
	}
}


void ASG_GameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (Game.IsValid())
	{
		Game->update(DeltaSeconds, SnakeInput);
	}
}

SnakeGame::Settings ASG_GameMode::MakeSettings() const
{
	SnakeGame::Settings GS;

#if WITH_EDITOR
	if (bOverrideUserSettings)
	{
		GS.gridSize = SnakeGame::Dim{ GridSize.X, GridSize.Y };
		GS.gameSpeed = GameSpeed;
	}
	else 
#endif
		if (auto* UserSettings = USG_GameUserSettings::GetSettings())
	{
		GS.gridSize = UserSettings->GridSize();
		GS.gameSpeed = UserSettings->GameSpeed();
	}
	
	GS.snake.defaultSize = SnakeDefaultSize;
	GS.snake.startPosition = SnakeGame::Grid::center(GS.gridSize.width, GS.gridSize.height);
	return GS;
}

void ASG_GameMode::SubscribeOnGameEvents()
{
	using namespace SnakeGame;

	Game->subscribeOnGameplayEvent([&](GameplayEvent Event)
		{
			switch (Event)
			{
			case SnakeGame::GameplayEvent::GameOver:
				UE_LOG(LogSnakeGameMode, Warning, TEXT("*******GAME OVER********"));
				UE_LOG(LogSnakeGameMode, Warning, TEXT("*******Score: %i********"), Game->score());
				SnakeVisual->Explode();
				FoodVisual->Hide();
				WorldUtils::SetUIIput(GetWorld(), true);
				break;
			case SnakeGame::GameplayEvent::GameCompleted:
				UE_LOG(LogSnakeGameMode, Warning, TEXT("*******GAME COMPLETED....really?!********"));
				UE_LOG(LogSnakeGameMode, Warning, TEXT("*******Score: %i********"), Game->score());
				break;
			case SnakeGame::GameplayEvent::FoodTaken:
				UE_LOG(LogSnakeGameMode, Warning, TEXT("*******FOOD TAKEN********"));
				FoodVisual->Explode();
				break;
			default:
				break;
			}
		});
}