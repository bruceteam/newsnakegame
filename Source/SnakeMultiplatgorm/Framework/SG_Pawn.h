// SnakeGamelifeEXEtrib

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SnakeMultiplatgorm/Core/SnakeTypes.h"
#include "SG_Pawn.generated.h"

class UCameraComponent;

UCLASS()
class SNAKEMULTIPLATGORM_API ASG_Pawn : public APawn
{
	GENERATED_BODY()

public:
	ASG_Pawn();

	void UpdateLocation(const SnakeGame::Dim& Dim, int32 CellSize, const FTransform& GridOrigin);

protected:
	UPROPERTY(VisibleAnywhere)
		USceneComponent* Origin;

	UPROPERTY(VisibleAnywhere)
		UCameraComponent* Camera;

private:
	SnakeGame::Dim Dim;
	int32 CellSize;
	FTransform GridOrigin;

	void OnViewportResized(FViewport* Viewport, uint32 Val);
};