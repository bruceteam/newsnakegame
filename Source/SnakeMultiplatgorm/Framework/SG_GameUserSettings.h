// SnakeGamelifeEXEtrib

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameUserSettings.h"
#include "SnakeMultiplatgorm/Core/SnakeTypes.h"
#include "SG_GameUserSettings.generated.h"

UENUM()
enum class EGameSpeed : uint8
{
	Slow = 0,
	Normal,
	Fast,
	LightSpeed
	//if u r adding smthg here, do not forget add same in TMap below
};

UENUM()
enum class EGridSize : uint8
{
	Size_35x13 = 0,
	Size_50x22,
	Size_70x26
	//if u r adding smthg here, do not forget add same in TMap below
};

UCLASS()
class SNAKEMULTIPLATGORM_API USG_GameUserSettings : public UGameUserSettings
{
	GENERATED_BODY()

public:
	static USG_GameUserSettings* GetSettings();

	TArray<FString> GameSpeedOptions() const
	{
		TArray<FString> SpeedNames;
		for (const auto& GameSpeed : GameSpeeds)
		{
			SpeedNames.Add(GameSpeed.Value.Name);
		}
		return SpeedNames;
	}

	FString GetCurrentGameSpeedOption() const { return CurrentSpeed.Name; }

	TArray<FString> GridSizeOptions() const
	{
		TArray<FString> GridNames;
		for (const auto& GridSize : GameSizes)
		{
			GridNames.Add(GridSize.Value.Name);
		}
		return GridNames;
	}

	FString GetCurrentGridSizeOption() const { return CurrentGridSize.Name; }

	void SaveSnakeSettings(EGameSpeed GameSpeed, EGridSize GridSize);
	EGameSpeed GameSpeedByName(const FString& Name) const;
	EGridSize GridSizeByName(const FString& Name) const;

	SnakeGame::Dim GridSize() const { return CurrentGridSize.Value; }
	float GameSpeed() const { return CurrentSpeed.Value; }
private:
	struct FSpeedData
	{
		FString Name;
		float Value;
	};

	const TMap<EGameSpeed, FSpeedData> GameSpeeds //
	{
		{
			EGameSpeed::Slow, {"Slow", 0.3f}
		}, //
		{
			EGameSpeed::Normal, {"Normal", 0.2f}
		}, //
		{
			EGameSpeed::Fast, {"Fast", 0.1f}
		}, //
		{
			EGameSpeed::LightSpeed, {"LightSpeed", 0.05f}
		}
	};

	struct FGridData
	{
		FString Name;
		SnakeGame::Dim Value;
	};

	const TMap<EGridSize, FGridData> GameSizes //
	{
		{
			EGridSize::Size_35x13, {"35 X 13", SnakeGame::Dim{35,13}}
		}, //
		{
			EGridSize::Size_50x22, {"50 X 22", SnakeGame::Dim{50,22}}
		}, //
		{
			EGridSize::Size_70x26, {"70 X 26", SnakeGame::Dim{70,26}}
		}, //
	};

	FSpeedData CurrentSpeed{GameSpeeds[EGameSpeed::Normal]};
	FGridData CurrentGridSize{GameSizes[EGridSize::Size_35x13]};
};
