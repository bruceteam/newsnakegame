// SnakeGamelifeEXEtrib


#include "SnakeMultiplatgorm/Framework/SG_GameUserSettings.h"

USG_GameUserSettings* USG_GameUserSettings::GetSettings()
{
    return GEngine ? Cast<USG_GameUserSettings>(GEngine->GetGameUserSettings()) : nullptr;
}

void USG_GameUserSettings::SaveSnakeSettings(EGameSpeed GameSpeed, EGridSize GridSize)
{
    CurrentSpeed = GameSpeeds[GameSpeed];
    CurrentGridSize = GameSizes[GridSize];
}

EGameSpeed USG_GameUserSettings::GameSpeedByName(const FString& Name) const
{
    const auto* Founded = Algo::FindByPredicate(GameSpeeds, //
        [Name](const TPair<EGameSpeed, FSpeedData>& GameSpeed) {return GameSpeed.Value.Name.Equals(Name); });

    return Founded ? Founded->Key : EGameSpeed::Normal;
}

EGridSize USG_GameUserSettings::GridSizeByName(const FString& Name) const
{
    const auto* Founded = Algo::FindByPredicate(GameSizes, //
        [Name](const TPair<EGridSize, FGridData>& GridSize) {return GridSize.Value.Name.Equals(Name); });

    return Founded ? Founded->Key : EGridSize::Size_35x13;
}
