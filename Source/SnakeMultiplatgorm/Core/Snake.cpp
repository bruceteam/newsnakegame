// SnakeGamelifeEXEtrib


#include "SnakeMultiplatgorm/Core/Snake.h"

using namespace SnakeGame;

Snake::Snake(const Settings::Snake& settings)
{
	checkf(settings.defaultSize >= 2, TEXT("Snake lenght is 2 small!!!"));

	const auto StartPos = settings.startPosition;
	for (uint32 i = 0; i < settings.defaultSize; ++i)
	{
		m_links.AddTail(Position(StartPos.x - i, StartPos.y));
	}
}

void SnakeGame::Snake::move(const Input& input)
{
	//can't move backward
	if (!m_lastInput.opposite(input))
	{
		m_lastInput = input;
	}

	m_links.GetTail()->GetValue() = m_links.GetHead()->GetValue();
	m_links.MoveTailAfterHead();
	m_links.GetHead()->GetValue() += Position(m_lastInput.x, m_lastInput.y);
}

void SnakeGame::Snake::increaseSize()
{
	m_links.AddTail(m_links.GetTail()->GetValue());
}

