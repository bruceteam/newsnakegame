// SnakeGamelifeEXEtrib

#pragma once

#include "CoreMinimal.h"
#include "SnakeTypes.h"

namespace SnakeGame
{

	class Snake
	{
	public:
		Snake(const Settings::Snake& settings);

		const TSnakeList& links() const { return m_links; }
		const Position head() { return m_links.GetHead()->GetValue(); }

		void move(const Input& input);
		void increaseSize();
	private:
		TSnakeList m_links;
		Input m_lastInput{Input::Default};
	};

}