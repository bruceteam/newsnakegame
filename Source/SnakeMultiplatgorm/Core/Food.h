// SnakeGamelifeEXEtrib

#pragma once

#include "CoreMinimal.h"
#include "SnakeTypes.h"

namespace SnakeGame
{

	class Food
	{
	public:
		Food() = default;

		void SetPosition(const Position& position);
		Position position() const;

	private:
		Position m_position{ Position::Zero };
	};

}
