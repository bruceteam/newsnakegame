// SnakeGamelifeEXEtrib


#include "SnakeMultiplatgorm/Core/Game.h"
#include "SnakeMultiplatgorm/Core/Grid.h"
#include "SnakeMultiplatgorm/Core/Snake.h"
#include "SnakeMultiplatgorm/Core/Food.h"

using namespace SnakeGame;

Game::Game(const Settings& settings, const TSharedPtr<IPositionRandomizer>& randomizer) : c_settings(settings)
{
	m_grid = MakeShared<Grid>(settings.gridSize, randomizer);
	m_snake = MakeShared<Snake>(settings.snake);
	m_food = MakeShared<Food>();

	updateGrid();
	generateFood();
}

void Game::update(float deltaSeconds, const Input& input)
{
	if (m_gameOver || !updateTime(deltaSeconds)) return;
	m_snake->move(input);

	if (died())
	{
		m_gameOver = true;
		dispatchEvent(GameplayEvent::GameOver);
		return;
	}

	if (foodTaken())
	{
		++m_score;
		m_snake->increaseSize();
		dispatchEvent(GameplayEvent::FoodTaken);
		generateFood();
	}

	updateGrid();
}



void Game::updateGrid()
{
	m_grid->update(m_snake->links().GetHead(), CellType::Snake);
	//m_grid->printDebug();
}

bool Game::updateTime(float deltaSeconds)
{
	m_gameTime += deltaSeconds;
	m_moveSeconds += deltaSeconds;
	if (m_moveSeconds < c_settings.gameSpeed) return false;

	m_moveSeconds = 0.0f;
	return true;
}

bool Game::died() const
{
	return m_grid->hitTest(m_snake->head(), CellType::Wall) || m_grid->hitTest(m_snake->head(), CellType::Snake);
}

void SnakeGame::Game::generateFood()
{
	Position foodPosition;
	if (m_grid->randomEmptyPosition(foodPosition))
	{
		m_food->SetPosition(foodPosition);
		m_grid->update(m_food->position(), CellType::Food);
	}
	else
	{
		m_gameOver = true;
		dispatchEvent(GameplayEvent::GameCompleted);
	}
}

bool Game::foodTaken() const
{
	return m_grid->hitTest(m_snake->head(), CellType::Food);
}

void Game::subscribeOnGameplayEvent(GameplayEventCallback callback)
{
	m_gameplayEventCallbacks.Add(callback);
}

void Game::dispatchEvent(GameplayEvent Event)
{
	for (const auto& callback : m_gameplayEventCallbacks)
	{
		if (callback)
		{
			callback(Event);
		}
	}
}