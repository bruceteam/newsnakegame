// SnakeGamelifeEXEtrib


#include "SnakeMultiplatgorm/UI/SG_GameOverWidget.h"
#include "Components/TextBlock.h"
#include "SnakeMultiplatgorm/World/SG_WorldUtils.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

void USG_GameOverWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (BackToMenuButton)
	{
		BackToMenuButton->OnClicked.AddDynamic(this, &USG_GameOverWidget::OnBackToMenu);
	}
}

void USG_GameOverWidget::UpdateScore(uint32 Score)
{
	if (ScoreText)
	{
		ScoreText->SetText(SnakeGame::WorldUtils::FormatScore(Score));
	}
}

void USG_GameOverWidget::SetResetKeyName(const FString& ResetGameKeyName)
{
	if (ResetGameText)
	{
		const FString ResetGameInfo = FString::Printf(TEXT("press [%s] to reset"), *ResetGameKeyName.ToLower());
		ResetGameText->SetText(FText::FromString(ResetGameInfo));
	}
}


void USG_GameOverWidget::OnBackToMenu()
{
	if (!MenuLevel.IsNull())
	{
		UGameplayStatics::OpenLevel(GetWorld(), FName(MenuLevel.GetAssetName()));
	}
}
