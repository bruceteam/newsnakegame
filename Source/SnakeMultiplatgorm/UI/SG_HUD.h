// SnakeGamelifeEXEtrib

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SG_HUD.generated.h"

class USG_GameplayWidget;
class USG_GameOverWidget;

UENUM()
enum class EUIMatchState : uint8
{
	GameInProgress = 0,
	GameOver,
	GameCompleted
};

namespace SnakeGame
{
	class Game;

}

UCLASS()
class SNAKEMULTIPLATGORM_API ASG_HUD : public AHUD
{
	GENERATED_BODY()

public:
	ASG_HUD();

	void SetModel(const TSharedPtr<SnakeGame::Game>& InGame);
	void SetInputKeyNames(const FString& ResetGameKeyName);
	virtual void Tick(float DeltaSeconds) override;
protected:
	UPROPERTY(EditDefaultsOnly, Category = "UI")
		TSubclassOf<USG_GameplayWidget> GameplayWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = "UI")
		TSubclassOf<USG_GameOverWidget> GameOverWidgetClass;

	virtual void BeginPlay() override;
private:
	UPROPERTY()
		TObjectPtr<USG_GameplayWidget> GameplayWidget;
	UPROPERTY()
		TObjectPtr<USG_GameOverWidget> GameOverWidget;
	UPROPERTY()
		TMap<EUIMatchState, TObjectPtr<UUserWidget>> GameWidgets;
	UPROPERTY()
		TObjectPtr<UUserWidget>CurrentWidget;

	TWeakPtr<SnakeGame::Game> Game;
	EUIMatchState MatchState;

	void SetUIMatchState(EUIMatchState InMatchState);
};
