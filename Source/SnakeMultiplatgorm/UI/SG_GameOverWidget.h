// SnakeGamelifeEXEtrib

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SG_GameOverWidget.generated.h"

class UTextBlock;
class UButton;

UCLASS()
class SNAKEMULTIPLATGORM_API USG_GameOverWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void UpdateScore(uint32 Score);
	void SetResetKeyName(const FString& ResetGameKeyName);
protected:
	UPROPERTY(meta = (BindWidget))
		TObjectPtr<UButton> BackToMenuButton;

	UPROPERTY(EditDefaultsOnly)
		TSoftObjectPtr<UWorld> MenuLevel;

	virtual void NativeOnInitialized() override;
private:
	UPROPERTY(meta = (BindWidget))
	TObjectPtr<UTextBlock> ScoreText;

	UPROPERTY(meta = (BindWidget))
	TObjectPtr<UTextBlock> ResetGameText;

	UFUNCTION()
		void OnBackToMenu();
};
