// SnakeGamelifeEXEtrib

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SG_GameplayWidget.generated.h"

class UTextBlock;

UCLASS()
class SNAKEMULTIPLATGORM_API USG_GameplayWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void SetGameTime(float Time);
	void UpdateScore(uint32 Score);
	void SetResetKeyName(const FString& ResetGameKeyName);
protected:
	UPROPERTY(meta = (BindWidget))
		TObjectPtr<UTextBlock> TimeText;

	UPROPERTY(meta = (BindWidget))
		TObjectPtr<UTextBlock> ScoreText;

	UPROPERTY(meta = (BindWidget))
	TObjectPtr<UTextBlock> ResetGameText;
};
