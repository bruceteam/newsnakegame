// SnakeGamelifeEXEtrib


#include "SnakeMultiplatgorm/UI/SG_HUD.h"
#include "SnakeMultiplatgorm/UI/SG_GameplayWidget.h"
#include "SnakeMultiplatgorm/UI/SG_GameOverWidget.h"
#include "SnakeMultiplatgorm/Core/Game.h"

ASG_HUD::ASG_HUD()
{
	PrimaryActorTick.TickInterval = 1.0f;
}

void ASG_HUD::BeginPlay()
{
	Super::BeginPlay();
	
	GameplayWidget = CreateWidget<USG_GameplayWidget>(GetWorld(), GameplayWidgetClass);
	check(GameplayWidget);
	GameWidgets.Add(EUIMatchState::GameInProgress, GameplayWidget);

	GameOverWidget = CreateWidget<USG_GameOverWidget>(GetWorld(), GameOverWidgetClass);
	check(GameOverWidget);
	GameWidgets.Add(EUIMatchState::GameOver, GameOverWidget);

	for (auto& [UIState, Widget] : GameWidgets)
	{
		if (Widget)
		{
			Widget->AddToViewport();
			Widget->SetVisibility(ESlateVisibility::Collapsed);
		}
	}
}


void ASG_HUD::SetModel(const TSharedPtr<SnakeGame::Game>& InGame)
{
	if (!InGame) return;
	using namespace SnakeGame;

	Game = InGame;
	SetUIMatchState(EUIMatchState::GameInProgress);

	GameplayWidget->UpdateScore(InGame->score());
	GameOverWidget->UpdateScore(InGame->score());

	InGame->subscribeOnGameplayEvent([&](GameplayEvent Event) 
		{
			switch (Event)
			{
			case SnakeGame::GameplayEvent::FoodTaken:
				GameplayWidget->UpdateScore(InGame->score());
				break;
			case SnakeGame::GameplayEvent::GameCompleted: [[fallthrough]];
			case SnakeGame::GameplayEvent::GameOver:
				GameOverWidget->UpdateScore(InGame->score());
				SetUIMatchState(EUIMatchState::GameOver);
				break;
			}
		});
}

void ASG_HUD::SetInputKeyNames(const FString& ResetGameKeyName)
{
	GameplayWidget->SetResetKeyName(ResetGameKeyName);
	GameOverWidget->SetResetKeyName(ResetGameKeyName);
}

void ASG_HUD::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (Game.IsValid() && MatchState == EUIMatchState::GameInProgress)
	{
		GameplayWidget->SetGameTime(Game.Pin()->gameTime());
	}
}

void ASG_HUD::SetUIMatchState(EUIMatchState InMatchState)
{
	if (CurrentWidget)
	{
		CurrentWidget->SetVisibility(ESlateVisibility::Collapsed);
	}

	if (GameWidgets.Contains(InMatchState))
	{
		CurrentWidget = GameWidgets[InMatchState];
		CurrentWidget->SetVisibility(ESlateVisibility::Visible);
	}

	MatchState = InMatchState;
}