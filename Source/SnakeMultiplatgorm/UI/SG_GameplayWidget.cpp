// SnakeGamelifeEXEtrib


#include "SnakeMultiplatgorm/UI/SG_GameplayWidget.h"
#include "Components/TextBlock.h"
#include "SnakeMultiplatgorm/World/SG_WorldUtils.h"

void USG_GameplayWidget::SetGameTime(float Time)
{
	if (TimeText)
	{
		TimeText->SetText(SnakeGame::WorldUtils::FormatSeconds(Time));
	}
}

void USG_GameplayWidget::UpdateScore(uint32 Score)
{
	if (ScoreText)
	{
		ScoreText->SetText(SnakeGame::WorldUtils::FormatScore(Score));
	}
}

void USG_GameplayWidget::SetResetKeyName(const FString& ResetGameKeyName)
{
	if (ResetGameText)
	{
		const FString ResetGameInfo = FString::Printf(TEXT("press [%s] to reset"), *ResetGameKeyName.ToLower());
		ResetGameText->SetText(FText::FromString(ResetGameInfo));
	}
}
