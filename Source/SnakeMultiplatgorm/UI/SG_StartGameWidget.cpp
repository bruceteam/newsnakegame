// SnakeGamelifeEXEtrib


#include "SnakeMultiplatgorm/UI/SG_StartGameWidget.h"
#include "Components/Button.h"
#include "Components/ComboBoxString.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "SnakeMultiplatgorm/Framework/SG_GameUserSettings.h"

void USG_StartGameWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	auto* UserSettings = USG_GameUserSettings::GetSettings();
	if (!UserSettings) return;


	if (StartGameButton)
	{
		StartGameButton->OnClicked.AddDynamic(this, &USG_StartGameWidget::OnStartGame);
	}
	
	if (CloseGameButton)
	{
		CloseGameButton->OnClicked.AddDynamic(this, &USG_StartGameWidget::OnCloseGame);
	}
	
	check(GameSpeedComboBox);
	check(GridSizeComboBox);

	GameSpeedComboBox->ClearOptions();
	for (const auto& Option : UserSettings->GameSpeedOptions())
	{
		GameSpeedComboBox->AddOption(Option);
	}
	GameSpeedComboBox->SetSelectedOption(UserSettings->GetCurrentGameSpeedOption());
	GameSpeedComboBox->OnSelectionChanged.AddDynamic(this, &USG_StartGameWidget::OnGameSpeedChanged);

	GridSizeComboBox->ClearOptions();
	for (const auto& Option : UserSettings->GridSizeOptions())
	{
		GridSizeComboBox->AddOption(Option);
	}
	GridSizeComboBox->SetSelectedOption(UserSettings->GetCurrentGridSizeOption());
	GridSizeComboBox->OnSelectionChanged.AddDynamic(this, &USG_StartGameWidget::OnGridSizeChanged);
}

void USG_StartGameWidget::OnStartGame()
{
	if (!GameLevel.IsNull())
	{
		UGameplayStatics::OpenLevel(GetWorld(), FName(GameLevel.GetAssetName()));
	}
}

void USG_StartGameWidget::OnCloseGame()
{
	UKismetSystemLibrary::QuitGame(GetWorld(), GetOwningPlayer(),EQuitPreference::Quit, false);
}

void USG_StartGameWidget::OnGameSpeedChanged(FString SelectedItem, ESelectInfo::Type SelectionType)
{
	if (SelectionType == ESelectInfo::OnMouseClick)
	{
		SaveSettings();
	}
}

void USG_StartGameWidget::OnGridSizeChanged(FString SelectedItem, ESelectInfo::Type SelectionType)
{
	if (SelectionType == ESelectInfo::OnMouseClick)
	{
		SaveSettings();
	}
}

void USG_StartGameWidget::SaveSettings()
{
	if (auto* UserSettings = USG_GameUserSettings::GetSettings())
	{
		const EGameSpeed GameSpeed = UserSettings->GameSpeedByName(GameSpeedComboBox->GetSelectedOption());
		const EGridSize GridSize = UserSettings->GridSizeByName(GridSizeComboBox->GetSelectedOption());
		UserSettings->SaveSnakeSettings(GameSpeed, GridSize);
	}
}
